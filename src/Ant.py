#coding= utf8
from random import randint as rand, seed as srand;

################################################################################
# @brief Représentation d'une fourmie
##
class Ant:
	m_name = '??';#!< Nom de la fourmi
	m_pos = '??';#!< Position actuel
	m_road = None;#!< Chemin parcourus
	m_road_weight = 0;#!< Somme des poids des chemins parcouru
	m_old_road = None;#!< Ancien chemin
	m_old_road_weight = 0;#!< Poid del'ancien chemin
	m_graph = None;#!< Graph sur lequel la fourmi se dépalce
	m_goHome = False;#!< La fourmi revient à son point de départ ?
	m_usePhero = True;#!< Utiliser le système de phéromones
	m_longevity = 0;#!< Age de la fourmi


	############################################################################
	# @brief Constructeur
	# @param[in] name		{Str} Nom de la fourmi
	# @param[in] pos		{Char} Position initial de la fourmi
	# @param[in,out] graph	{Graph} Le graph où se déplace la froumi
	# @param[in] usePhero	{Bool} Utiliser le système de phéromones ?
	# @return[NONE]
	##
	def __init__( self, name, pos, graph, usePhero=True ):
		self.m_pos = pos;
		self.m_name = name;
		self.m_road = [];
		self.m_road_weight = 0;
		self.m_old_road = None;
		self.m_old_road_weight = 0;
		self.m_graph = graph;
		self.m_goHome = False;
		self.m_usePhero = usePhero;
		self.m_longevity = 0;


	############################################################################
	# @brief Permet de déplacer la fourmi
	# @param[in] newPos		{Char} Nouvelle position de la fourmi
	# @return[NONE]
	#
	# @note L'ancienne position va être enregistrée
	##
	def moveTo( self, newPos ):
		self.m_road.append(self.m_pos);
		self.m_road_weight += self.m_graph[self.m_pos+newPos]['weight'];
		self.m_pos = newPos;


	############################################################################
	# @brief Permet de choisir la route. (C'est ici que les algo sont implémentés)
	# @return[NONE]
	##
	def chooseRoad( self ):
		"""
		Chaque fourmi a une mémoire implémentée par une liste de
		villes déjà visitées. Cela permet de garantir qu’aucune fourmi ne
		visitera deux fois une même ville au cours de sa recherche.

		Une fourmi {k} placée sur la ville i à l’instant {t} va choisir sa ville {j}
		de destination (avec une probabilité qui est fonction de la visibilité
		dij (distance entre la ville {i} et {j}) de cette ville et de la quantité de
		phéromones pij(t) déposée sur l’arc reliant ces deux villes.
		"""

		self.m_longevity+=1;

		if self.m_goHome:
			self.goHome();
			return;

		# Sélection des routes possibles.
		# Sélection des routes vers des points non visité
		roads = [];
		for pts in self.m_graph.m_points:
			if (self.m_pos != pts) and (pts not in self.m_road):
				roads.append(self.m_pos+pts);

		if len(roads) == 0:
			# On garde le meilleur chemin en mémoire
			if self.m_road_weight < self.m_old_road_weight or self.m_old_road == None:
				self.m_old_road_weight = self.m_road_weight;
				self.m_old_road = list(self.m_road)+[self.m_pos];

			self.m_goHome = True;
			self.goHome();
			return;

		dataOrder = {};

		# On sélectionne les routes en fonction de leur "poid"
		for r in roads:
			dataOrder[r] = 101-(self.m_graph[r]['weight']*100/self.m_graph.m_max_weight);

		if self.m_usePhero:
			maxPhero = self.m_graph.maxPhero();
			if maxPhero != 0:
				for r in roads:
					dataOrder[r] = (dataOrder[r]+((self.m_graph[r]['phero']*100)/maxPhero))/2;

		# Création de la "barre" de pourcentage:
		# [1,1,1,1,1,5,5,5,5,5,...]
		selector = [];
		for item in dataOrder:
			if dataOrder[item] < 1:
				dataOrder[item] = 1;
			for i in xrange(0,int(round(dataOrder[item]))):
				selector.append(item);

		best = selector[rand(0,len(selector)-1)];
		self.moveTo(best.replace(self.m_pos, ''));


	############################################################################
	# @brief Permet d'ordonner à la fourmi de retourner en arrière
	# @return[NONE]
	##
	def goHome( self ):
		currentPos = self.m_road[len(self.m_road)-1];
		if self.m_usePhero:
			self.m_graph[self.m_pos+currentPos]['phero'] += self.m_graph.m_worse_road-self.m_road_weight;
		self.m_pos = currentPos;
		del self.m_road[len(self.m_road)-1];

		if len(self.m_road) == 0:
			self.m_road_weight = 0;
			self.m_goHome = False;


	############################################################################
	# @brief Permet de choisir la route. (C'est ici que les algo sont implémentés)
	# @return[NONE]
	#
	# @note Utilisation de l'ago génétique
	##
	def chooseRoad_gen( self ):
		pass;


	############################################################################
	# @brief Permet d'afficher correctement la class Ant
	# @return {Str} L'affichage d'une fourmi
	##
	def __str__(self):
		return self.m_name+': >'+str(self.m_pos)+'< '+str(self.m_road)+'...'+str(self.m_road_weight);
	def __repr__(self):
		return self.__str__();
