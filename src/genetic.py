#!/usr/bin/python
#coding= utf8
import sys, os;
import math;
from random import randint as rand, seed as srand;
from time import time;

# NOTE: PC = Par Cycle
__LONGEVITY__		= 5;# Cycle
__POP_INIT__		= 20;# Individus
__NB_CROSSING_PC__	= 6;
__NB_MUT_PC__		= 3;
__INTVAL__			= [0,100];
__FIND_ME__			= rand(__INTVAL__[0],__INTVAL__[1]);

__FOLLOW_PARENT__	= False;
__FOLLOW_MUTATION__	= False;


################################################################################
# @brief Main
# @param[in] argv		{Tuple} Données de la ligne de commande
# @return {INT} 0 si pas d'erreur.
##
def main( argv ):
	pop = genPOP(__POP_INIT__);
	iter = 0;
	while not hasSecret(pop):
		iter+=1;
		older(pop);
		cleanPop(pop);
		crossing(pop, __NB_CROSSING_PC__);
		mutation(pop, __NB_MUT_PC__);
		selectPop(pop);

	print 'NBIter:'+str(iter)+', Find:'+str(__FIND_ME__);
	print str(pop).replace(': '+str(__FIND_ME__)+',', ': \033[1;34;40m'+str(__FIND_ME__)+'\033[0m,');
	return 0;



################################################################################
# @brief Permet de générer une population
# @param[in] nb		{uint} Nombre d'individu à créer
# @return {List} Une population de {nb} individu.
##
def genPOP( nb ):
	pop = [];
	for i in xrange(0,nb):
		pop.append(Indie(name=chr(ord('a')+i)));
	return pop;


################################################################################
# @brief Différence entre l'individu et la valeur à chercher
# @param[in] indie		{Indie} L'individu
# @return La différence absolue: | indi - __find_me__ |
##
def fitness( indie ):
	return math.fabs(indie - __FIND_ME__);


################################################################################
# @brief Permet de faire vieillir une population
# @param[in,out] pop		{List} Liste d'individu (appelé population)
# @return[NONE]
##
def older( pop ):
	for indie in pop:
		indie.older();

################################################################################
# @brief Permet de supprimer tout les membres de la populations qui sont trop vieux
# @param[in,out] pop		{List} Liste d'individu (appelé population)
# @return[NONE]
##
def cleanPop( pop ):
	i=0
	while( i < len(pop) ):
		if pop[i].isTooOld():
			del pop[i];
			continue;
		i+=1;


################################################################################
# @brief Permet de créer des enfants.
# @param[in,out] pop	{List(Indie)} Liste d'individu (appelé population)
# @param[in] nbCrossing	{Uint} Nombre de nouveau enfant (Default: rand)
# @return[NONE]
##
def crossing( pop, nbCrossing=None ):
	nbPop = len(pop);
	if nbCrossing == None:
		nbCrossing = rand(1,nbPop+1);

	for i in xrange(0, nbCrossing):
		# On veux un parent vivant
		while 1:
			p1 = rand(0,nbPop-1);# -1 sinon on prend un enfant + risque de segfault
			if not pop[p1].isTooOld():
				break;

		p2 = rand(0,nbPop-1);# -1 sinon on prend un enfant + risque de segfault
		# On s'assure que p2 n'est pas égal à p1
		# On veux 2 parents différent et vivant
		while p1 == p2 or pop[p2].isTooOld():
			p2 = rand(0,nbPop-1);# -1 sinon on prend un enfant + risque de segfault

		p1 = pop[p1];
		p2 = pop[p2];
		pop.append(Indie(chromosome=(p1.m_chromosome+p2.m_chromosome)/2, parents=(p1, p2), name=p1.m_name.replace(',','')+','+p2.m_name.replace(',',''), longevity=0));


################################################################################
# @brief Permet de faire muter la population
# @param[in,out] pop	{List(Indie)} Liste d'individu (appelé population)
# @param[in] nbMut		{Uint} Nombre de mutation (Default: rand)
# @return[NONE]
##
def mutation( pop, nbMut=None ):
	if nbMut == None:
		nbMut = rand(0, len(pop)-1);
	mutationPop = [];
	for i in xrange(0,nbMut):
		p = rand(0,len(pop)-1);
		if p not in mutationPop:
			pop[p].mutation();
			mutationPop.append(p);


################################################################################
# @brief Permet de de déterminé si la population contient un indie avec le chromosome demandé
# @param[in] pop	{List(Indie)} Liste d'individu (appelé population)
# @return{Bool} TRUE si un indie a le chromosome recherché. FALSE sinon
##
def hasSecret( pop ):
	for indie in pop:
		if indie.m_chromosome == __FIND_ME__:
			return True;
	return False;


################################################################################
# @brief Permet de séléctionner q'une partie de la population
# @param[in,out] pop	{List(Indie)} Liste d'individu (appelé population)
# @return[NONE]
##
def selectPop( pop ):
	tab = [];
	maxChromosome = __INTVAL__[1];

	for indie in pop:
		for i in xrange(0, maxChromosome-indie.m_chromosome):
			tab.append(indie);

	del pop[:];

	for i in xrange(0,__POP_INIT__):
		while len(tab):
			p = rand(0, len(tab)-1);
			if tab[p] not in pop:
				indie = tab[p];
				pop.append(indie);
				j = p;
				while j >= 0 and tab[j] == indie:
					j -=1;
				j+=1;
				while j < len(tab) and tab[j] == indie:
					del tab[j];
				break;


class Indie:
	m_chromosome = 0;#!<	{Uint} Chromosome de l'individu
	m_longevity = 0;#!<		{Uint} Longévité
	m_parents = None;#!<	{Tuple} Parents
	m_name = None;#!<		{Str} Nom de l'individu
	m_mutations = None;#!<	{List} L'individu a muté

	###########################################################################
	# @brief Constructeur
	# @param[in] chromosome	{Int} La valeur du chromosome. (Default: rand)
	# @param[in] longevity	{Int} L'age. (Default: rand)
	# @param[in] parents	{Tuple} Les parents. (Default: None)
	# @param[in] name		{Str} Nom de l'individu
	# @return[NONE]
	##
	def __init__(self, chromosome=None, longevity=None, parents=None, name=None):
		if chromosome == None:
			chromosome = rand(__INTVAL__[0],__INTVAL__[1]);
		self.m_chromosome = chromosome;

		if longevity == None:
			longevity = rand(0,__LONGEVITY__-1);
		self.m_longevity = longevity;

		if __FOLLOW_PARENT__:
			self.m_parents = parents;

		if len(name) >= 20:
			name = name[:20]+'...';
		self.m_name = name;

		if __FOLLOW_MUTATION__:
			self.m_mutations = [];
		else:
			self.m_mutations = 'None';

	###########################################################################
	# @brief Permet de faire vieillir l'individu
	# @return[NONE]
	##
	def older(self):
		self.m_longevity += 1;

	###########################################################################
	# @brief Permet d'afficher une représentation Texte de l'objet
	# @return {String}
	##
	def __str__(self):
		if self.isTooOld():
			return '(%s: %d, dead(%d), Mut<%s>)\n'%(self.m_name, self.m_chromosome, self.m_longevity, str(self.m_mutations));
		return '(%s: %d, live(%d), Mut<%s>)\n'%(self.m_name, self.m_chromosome, self.m_longevity, str(self.m_mutations));

	###########################################################################
	# @brief Permet d'afficher une représentation Texte de l'objet
	# @return {String}
	##
	def __repr__( self ):
		return self.__str__();

	###########################################################################
	# @brief Permet de transformer la classe en INT
	# @return {Int}
	##
	def __int__(self):
		return self.m_chromosome;

	###########################################################################
	# @brief Permet de soustraire m_chromosome à chromosome
	# @param[in] chromosome	{int} chromosome à soustraire à m_chromosome
	# @return {Int}
	#
	# @note NE CHANGE PAS m_chromosome
	##
	def __sub__(self, chromosome):
		return self.m_chromosome-chromosome;

	###########################################################################
	# @brief Permet de savoir si l'individu est trop vieux
	# @return {Bool} TRUE si trop vieux. FALSE sinon
	##
	def isTooOld(self):
		return self.m_longevity >= __LONGEVITY__;

	def mutation(self):
		if __FOLLOW_MUTATION__:
			self.m_mutations.append(self.m_chromosome);
		self.m_chromosome = rand(__INTVAL__[0],__INTVAL__[1]);


if __name__ == '__main__':
	ret = main(sys.argv);
	if ret:
		sys.exit(ret);
