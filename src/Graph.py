#coding= utf8
from math import sqrt, pow as mathPow;

################################################################################
# @brief Représentation d'un Graph
# http://stackoverflow.com/questions/9163940/python-property-getter-setter-confusion
##
class Graph(object):
	m_graph = None;#!< Les données sur le graph: FORMAT: { 'AB': {'weight': uint, 'phero': uint}, ... }
	m_nbPoint = 0;#!< Nombre de points
	m_points = None;#!< Liste des points du graph
	m_max_weight = 0;#!< Poid maximal pour un arc
	m_worse_road = 0;#!< Pire route
	m_bestRoad = {'weight':9999, 'road':[]};#!< Meilleur route
	m_has2DPoints = False;#!< Les données du graph sont des coordonnées ?


	############################################################################
	# @brief Constructeur
	# @param[in] graph	{Dict} Les données sur le graph: FORMAT: { 'AB': {'weight': uint, 'phero': uint}, ... }
	# @return[NONE]
	##
	def __init__( self, graph ):
		self.m_graph = dict(graph);
		self.m_max_weight = 0;
		self.m_worse_road = 0;
		self.m_bestRoad = {'weight':9999, 'road':[]};#!< Meilleur route
		self.m_has2DPoints = False;
		self.transform2DPoint();
		self.extractGraphInfo();
		# Utile pour l'affichage 2D SDL
		self.make2DPosition();


	############################################################################
	# @brief Permet d'obtenir des info sur une liaison.
	# @param[in] currentPos		{Char} Point 1
	# @param[in] to				{Char} Point 2
	# @return {Dict} Format {'weight': uint, 'phero': uint}
	#
	# @note getInformation('A','B') == getInformation('B','A')
	# @warning NE TEST PAS l'EXISTANCE des points. Si un des points n'existe pas => SEGFAULT
	##
	def getInformation( self, currentPos, to ):
		if currentPos+to in self.m_graph:
			return self.m_graph[currentPos+to];
		return self.m_graph[to+currentPos];


	############################################################################
	# @brief Permet d'extraire des info sur la graph: le nombre de points et liste des points
	# @return[NONE]
	#
	# @note Si {m_graph} vient à changer, le calule ne sera pas changé
	##
	def extractGraphInfo( self ):
		ret = {};
		for i in self.m_graph:
			ret[i[0]] = 1;
			ret[i[1]] = 1;
			if self.m_graph[i]['weight'] > self.m_max_weight:
				self.m_max_weight = self.m_graph[i]['weight'];
		self.m_points = [];
		for i in ret:
			self.m_points.append(i);
		self.m_nbPoint = len(ret);


	############################################################################
	# @brief Permet d'obtenir le niveau de phéromone le plus haut du graph
	# @return {uint} Le niveau de phéromone le plus haut
	##
	def maxPhero( self ):
		max_phero = 0;
		for i in self.m_graph:
			if self.m_graph[i]['phero'] > max_phero:
				max_phero = self.m_graph[i]['phero'];
		return max_phero;


	############################################################################
	# @brief Permet d'accèder directement au contenu du graph
	# @param[in] idx	{Str} L'arc
	# @return {Dict: {'weight':xxxx, 'phero':yyyy}}
	#
	# @warning Si l'index n'existe pas => segfault
	##
	def __getitem__( self, idx ):
		if idx in self.m_graph:
			return self.m_graph[idx];
		return self.m_graph[idx[1]+idx[0]];


	############################################################################
	# @brief Permet d'accèder directement au contenu du graph
	# @param[in] idx	{Str} L'arc
	# @param[in] val	{Str} Nouvelle valeur pour l'arc
	# @return[NONE]
	##
	def __setitem__( self, idx, val ):
		if idx in self.m_graph:
			self.m_graph[idx] = val;
		self.m_graph[idx[1]+idx[0]] = val;


	############################################################################
	# @brief On applique du vent sur le graph => On éfface les pheromones
	# @param[in,opt] reduce_phero	{uint} Quantité de phéromones à supprimer à chaque arrête
	# @return[NONE]
	##
	def wind( self, reduce_phero=1 ):
		for i in self.m_graph:
			self.m_graph[i]['phero'] -= reduce_phero;
			if self.m_graph[i]['phero'] < 0:
				self.m_graph[i]['phero'] = 0;


	############################################################################
	# @brief Permet d'afficher correctement la class Graph
	# @return {Str} L'affichage d'un graph
	##
	def __str__( self ):
		filt = {};
		for i in self.m_graph:
			filt[i] = self.m_graph[i]['phero'];

		ret = [];
		for key,val in sorted(filt.iteritems(), key=lambda (k,v): v, reverse=True):
			ret.append({key:{'phero':'%.2f'%val,'weight':'%.2f'%self.m_graph[key]['weight']}});
			#ret[len(ret)][key] = val;
		return str(ret);


	############################################################################
	# @brief Permet d'initialiser la liste des coordonnées 2D
	# @return[NONE]
	#
	# @note Commande sans effet si les points 2D ont été auto-généré
	##
	def make2DPosition( self ):
		if self.m_has2DPoints:
			return ;
		self.m_2dCoord = {};
		for pts in self.m_points:
			self.m_2dCoord[pts] = {};


	############################################################################
	# @brief Permet de détermier si l'arc {arc} fait parti du chemin le plus cours
	# @param[in] arc	{Str} Arc a analyser (Format: 'AB')
	# @return {Bool} True si l'arc {arc} fait parti du chemin le plus court
	##
	def isInBestRoad( self, arc ):
		pts1 = arc[0:(len(arc)-1)/2+1]
		pts2 = arc[(len(arc)-1)/2+1:];

		for i in xrange(0,len(self.m_bestRoad['road'])-1):
			if (
				(self.m_bestRoad['road'][i] == pts1 and self.m_bestRoad['road'][i+1] == pts2)
				or
				(self.m_bestRoad['road'][i] == pts2 and self.m_bestRoad['road'][i+1] == pts1)
			):
				return True;
		return False;


	############################################################################
	# @brief Permet de transfomer un nuage de point en graph utilisable
	# @return[NONE]
	#
	# @note Transforme m_graph du format { A:[0,0], B:[1,1], ... } en { AB: {weight:1, phero:0}, ...}
	##
	def transform2DPoint( self ):
		if type(self.m_graph.itervalues().next()) == dict:
			return ;

		self.m_2dCoord = {};
		finalGraph = {};
		for i in self.m_graph:
			for j in self.m_graph:
				if (i != j) and (i+j not in finalGraph) and (j+i not in finalGraph):
					finalGraph[i+j] = {
						'weight': sqrt(
							mathPow(self.m_graph[i][0]-self.m_graph[j][0],2)
							+
							mathPow(self.m_graph[i][1]-self.m_graph[j][1],2)
						),
						'phero' :0
					};
			self.m_2dCoord[i] = {
				'coord': self.m_graph[i],
				'ptr': None
			}
		self.m_has2DPoints = True;
		self.m_graph = finalGraph;

