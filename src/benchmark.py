#!/usr/bin/python
#coding= utf8

from algo_ant import main as algo;
import sys, os;
from time import time;
from multiprocessing.pool import ThreadPool;

__NB_TRY__ = 20;#!< Nombre de tests sur chaque cas


# NE PAS TOUCHER
__RESULTS__ = {};#!< Resultats finaux
__DETAILS__ = {};#!< Resultats détaillé
__TEST__ = 0;#!< ID du test
__THREADS__ = None;#ThreadPool(10);


################################################################################
# @brief Main
# @return[NONE]
##
def main():
	begin = time();
	test('Algo des fourmies', use_prero=True, use_genetic=False);
	test('Algo des fourmies, makeRandomWind=True', use_prero=True, use_genetic=False, makeRandomWind=True);
	test('Algo des fourmies, disableWind=True', use_prero=True, use_genetic=False, disableWind=True);
	test('Algo des fourmies, use_prero=False', use_prero=False, use_genetic=False, disableWind=True);
	test('Algo genetique', use_prero=False, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=2, removeOldAnt=True);
	test('Algo genetique, use_prero=True', use_prero=True, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=2, removeOldAnt=True);
	test('Algo genetique, use_prero=True, makeRandomWind=True', use_prero=True, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=2, removeOldAnt=True, makeRandomWind=True);
	test('Algo genetique, use_prero=True, disableWind=True', use_prero=True, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=2, removeOldAnt=True, disableWind=True);
	test('Algo genetique, use_prero=True, disableWind=False', use_prero=True, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=2, removeOldAnt=True, disableWind=False);
	test('Algo genetique, removeOldAnt=False', use_prero=False, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=2, removeOldAnt=False);
	test('Algo genetique, isCrossing=False, removeOldAnt=False', use_prero=False, use_genetic=True, isCrossing=False, nbMutation=2, nbCrossing=2, removeOldAnt=False);
	test('Algo genetique, nbMutation=0', use_prero=False, use_genetic=True, isCrossing=True, nbMutation=0, nbCrossing=2, removeOldAnt=True);
	test('Algo genetique, nbMutation=10', use_prero=False, use_genetic=True, isCrossing=True, nbMutation=10, nbCrossing=2, removeOldAnt=True);
	test('Algo genetique, nbCrossing=10', use_prero=False, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=10, removeOldAnt=True);
	test('Algo genetique, nbCrossing=20', use_prero=False, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=20, removeOldAnt=True);
	test('Algo genetique, nbCrossing=10, removeOldAnt=False', use_prero=False, use_genetic=True, isCrossing=True, nbMutation=2, nbCrossing=10, removeOldAnt=False);

	print
	for key,val in sorted(__RESULTS__.items(), key=lambda res: res[1]):
		print key,':',val;
		#print '\t',__DETAILS__[key];

	print 'Execute en '+str(round(time()-begin))+'secs';


################################################################################
# @brief Permet de lancer des tests sur algo_ant avec des paramètres précis
# @param[in] key	{Str} Clé a utiliser dans le dico
# @param ...
# @return[NONE]
##
def test( key, use_prero=True, use_genetic=False, isCrossing=True, nbMutation=2, nbCrossing=2, removeOldAnt=True, disableWind=False, makeRandomWind=False ):
	global __DETAILS__,__TEST__,__RESULTS__;

	__PARAM__ = {
		'nbIter':		100,#!< Nombre max d'iter
		'nbAnts':		10,#!< Nombre de fourmi. Doit être supperieur à len(__GRAPH__)
		'use_genetic':	use_genetic,#!< Utiliser l'ago génétique ?
		'use_phero':	use_prero,#!< Activer le système de phéromones ?
		'genetic':{
			'isCrossing':	isCrossing,#!< Activer la réplication ?
			'nbMutation':	nbMutation,#!< Nombre de mutation
			'nbCrossing':	nbCrossing,#!< Nombre de fourmi à générer
			'removeOldAnt':	removeOldAnt,#!< Supprimer les vieilles fourmis ?
			'longevity':	10#!< Durée de vie des fourmis
		},
		'disablePrint':	True,#!< Désactiver l'affichage
		'disableWind':	disableWind,#!< Désactiver le vent
		'makeRandomWind': makeRandomWind
	};

	print 'Test: <'+key+'>';

	if __THREADS__:
		def tmp_func( i ):
			return algo(None, returnIter=True, disablePrint=True, disableWind=disableWind, params=__PARAM__);

		__DETAILS__[key] = __THREADS__.map(tmp_func, range(__NB_TRY__));
		__RESULTS__[key] = sum(__DETAILS__[key])/__NB_TRY__;
		return ;


	nbIter = 0;
	__DETAILS__[key] = [];
	for i in xrange(0, __NB_TRY__):
		print 'Test n°',__TEST__+1;
		tmp = algo(None, returnIter=True, disablePrint=True, disableWind=disableWind, params=__PARAM__);
		nbIter += tmp;
		__DETAILS__[key].append(tmp);
		__TEST__+=1;

	__RESULTS__[key] = nbIter/__NB_TRY__;


if __name__ == '__main__':
	main();
