#!/usr/bin/python
#coding= utf8
################################################################################
# Ce fichier contient l'ago de base pour le déplacement des fourmis.
# C'est ici que tout le travail est effectué
#
##
import sys, os, math;
from random import randint as rand, seed as srand;
from time import time;
from Graph import Graph;
from Ant import Ant;

__GRAPH_STOP__ = 27.56;#!< Arrêter la recherche si un chemin vaux cette valeur
__GRAPH__ = {
	'A': [0,0],
	'B': [1,2],
	'C': [-1,-5],
	'D': [0,-4],
	'E': [-8,7],
	'F': [5,5],
	'G': [-5,-3],
};


__GRAPH2__ = {
	'AB': {
		'weight': 1,
		'phero':0
	},
	'AC': {
		'weight': 3,
		'phero':0
	},
	'AD': {
		'weight': 2,
		'phero':0
	},
	'AE': {
		'weight': 5,
		'phero':0
	},

	################
	'BC': {
		'weight': 1,
		'phero':0
	},
	'BD': {
		'weight': 2,
		'phero':0
	},
	'BE': {
		'weight': 1,
		'phero':0
	},

	################
	'CD': {
		'weight': 8,
		'phero':0
	},
	'CE': {
		'weight': 7,
		'phero':0
	},

	################
	'DE': {
		'weight': 5,
		'phero':0
	},
};


# On met une fonction temporaire ici
def toPrint( gr ): pass;


################################################################################
# @brief Main
# @param[in] argv				{Tuple} Données de la ligne de commande
# @param[in,opt] returnIter		{Bool} Retourner le nombre d'itéreation ?
# @param[in,opt] disablePrint	{Bool} Désactiver l'affichage ?
# @param[in,opt] disableWind	{Bool} Désactiver le vent ?
# @param[in,opt] toPrintFunct	{Func} Fonction d'affichage a utiliser
# @param[in,out] graph			{Graph} Le graph a utiliser
# @param[in] params				{Dict} Les paramètres pures
# @return {INT} 0 si pas d'erreur.
##
def main( argv, returnIter=False, disablePrint=False, disableWind=False, toPrintFunct=None, graph=None, params=None ):
	global toPrint, __GR_INST__;
	gr = None;
	if graph != None:
		gr = graph;
	else:
		gr = Graph(__GRAPH__);

	__PARAM__ = {
		'nbIter':		100,#!< Nombre max d'iter
		'nbAnts':		10,#!< Nombre de fourmi. Doit être supperieur à len(__GRAPH__)
		'use_genetic':	False,#!< Utiliser l'ago génétique ?
		'use_phero':	True,#!< Activer le système de phéromones ?
		'genetic':{
			'isCrossing':	True,#!< Activer la réplication ?
			'nbMutation':	2,#!< Nombre de mutation
			'nbCrossing':	2,#!< Nombre de fourmi à générer
			'removeOldAnt':	True,#!< Supprimer les vieilles fourmis ?
			'longevity':	10#!< Durée de vie des fourmis
		},
		'disablePrint':	disablePrint,#!< Désactiver l'affichage
		'disableWind':	disableWind,#!< Désactiver le vent
		'makeRandomWind': False
	};

	if params != None:
		__PARAM__ = params;

	if argv != None and toPrintFunct == None and len(argv) >= 1:
		parseArgv(argv, __PARAM__);
		print 'Valeurs par defaut:';
		printDict(__PARAM__, 30);
		print
		print 'Note: -help pour voir l aide !';
		print 'Appuiez sur une touche pour continuer...';
		raw_input();


	if __PARAM__['nbAnts'] <= gr.m_nbPoint:
		print 'Le nombre de fourmi ('+str(__PARAM__['nbAnts'])+') doit être > au nombre de ville ('+str(gr.m_nbPoint)+')';
		return 0;


	lstAnts = [];

	if toPrintFunct != None and hasattr(toPrintFunct, '__call__'):
		toPrint = toPrintFunct;

	# On pose les fourmis aléatoirement sur les points
	for i in xrange(0,__PARAM__['nbAnts']):
		lstAnts.append(Ant('F'+str(i), gr.m_points[rand(0,len(gr.m_points)-1)], gr, __PARAM__['use_phero']));

	nbIter = __PARAM__['nbIter'];

	for i in xrange(0, __PARAM__['nbIter']):
		for j in xrange(0, gr.m_nbPoint-1):
			if not disablePrint:
				print 'Iter '+str(i+1)+':'+str(j+1);
			for ant in lstAnts:
				ant.chooseRoad();
				if not disablePrint:
					print '\t'+str(ant);
		if not disableWind:
			if __PARAM__['makeRandomWind']:
				gr.wind(rand(0,10));
			else:
				gr.wind();
		if not disablePrint:
			print gr;
			toPrint(gr);
		gr.m_worse_road = 0;
		nbMutation = __PARAM__['genetic']['nbMutation'];
		for a in lstAnts:
			if nbMutation > 0 and rand(0,1):
				mutation(a, __PARAM__);
				nbMutation-=1;
			if a.m_road_weight > gr.m_worse_road:
				gr.m_worse_road = a.m_road_weight;
			if len(a.m_road) and gr.m_bestRoad['weight'] > a.m_road_weight:
				gr.m_bestRoad['weight'] = a.m_road_weight;
				gr.m_bestRoad['road'] = list(a.m_road);
				gr.m_bestRoad['road'].append(a.m_pos);

		if gr.m_worse_road != 0:
			makeAbaby(lstAnts, gr.m_worse_road, __PARAM__);
			removeOldAnt(lstAnts, __PARAM__);

		# On s'arrête si on a trouvé le meilleur chemin
		if round(gr.m_bestRoad['weight'],2) <= __GRAPH_STOP__:
			if not disablePrint:
				print 'Nombre d iteration:',i+1;
			nbIter = i+1;
			break;

	if not disablePrint:
		print '-------- FIN --------';
		print gr.m_bestRoad;
	if returnIter:
		return nbIter;



################################################################################
# @brief Execute une mutation sur une fourmi
# @param[in,out] a		{Ant} La fourmi a alterer
# @param[in] __PARAM__	{Dict} Les paramètres du système
# @return[NONE]
##
def mutation( a, __PARAM__ ):
	if not __PARAM__['use_genetic'] or not len(a.m_road):
		return ;

	road = a.m_road+[a.m_pos];

	# On prend 2 sommet aléatoirement
	sommet1 = rand(0,len(road)-1);
	sommet2 = rand(0,len(road)-1);
	while sommet2 == sommet1:
		sommet2 = rand(0,len(road)-1);

	# Swap
	if sommet1 == len(a.m_road):
		a.m_pos,a.m_road[sommet2] = a.m_road[sommet2],a.m_pos;
	elif sommet2 == len(a.m_road):
		a.m_road[sommet1],a.m_pos = a.m_pos,a.m_road[sommet1];
	else:
		a.m_road[sommet1],a.m_road[sommet2] = a.m_road[sommet2],a.m_road[sommet1];

	# On reset le poids
	a.m_road_weight = 0;

	# On relance le calcule du poids
	for i in xrange(0, len(road)-1):
		a.m_road_weight += a.m_graph[road[i]+road[i+1]]['weight'];


################################################################################
# @brief On créer des bébé fourmi XD
# @param[in,out] antList		{List<Ant>} List de fourmi
# @param[in] worseRoadWeight	{uint} Pire valeur de chemin
# @param[in] __PARAM__			{Dict} Les paramètres du système
# @return[NONE]
##
def makeAbaby( antList, worseRoadWeight, __PARAM__ ):
	if not __PARAM__['use_genetic'] or not __PARAM__['genetic']['isCrossing']:
		return;
	percent = {};

	if len(antList) <= 1:
		return;

	# On donne une chance equitable a tout le monde
	chance = [];
	if len(antList) != 2:
		for a in antList:
			percent[a] = 101 - (a.m_road_weight*100/worseRoadWeight);

		for i in percent:
			for x in xrange(0,int(percent[i])):
				chance.append(i);
	else:
		chance = list(antList);

	# Bébé land
	nbCrossing = __PARAM__['genetic']['nbCrossing'];
	while nbCrossing > 0:
		a1 = rand(0,len(chance)-1);
		a2 = rand(0,len(chance)-1);
		while a1 == a2:
			a2 = rand(0,len(chance)-1);

		a = crossing(chance[a1],chance[a2]);
		if isGoodRoad(a.m_old_road):
			nbCrossing-=1;
			antList.append(a);


################################################################################
# @brief On créer un bébé fourmi XD
# @param[in] a1		{Ant} Un papa/maman fourmi
# @param[in] a2		{Ant} Un papa/maman fourmi
# @return {Ant} Un bébé fourmi
##
def crossing( a1, a2 ):
	finalRoad = '';

	a1_road = a1.m_road+[a1.m_pos];
	if a1.m_old_road:
		a1_road = a1.m_old_road;

	a2_road = a2.m_road+[a2.m_pos];
	if a2.m_old_road:
		a2_road = a2.m_old_road;

	size = len(a1_road);

	val  = rand(0,size-1);
	if rand(0,2):
		finalRoad += a1_road[val];
	else:
		finalRoad += a2_road[val];

	while len(finalRoad) < size:
		if rand(0,2):
			for i in xrange(0, size):
				if a1_road[i] == finalRoad[-1]:
					finalRoad += a1_road[(i+1)%size];
					break;
		else:
			for i in xrange(0, size):
				if a2_road[i] == finalRoad[-1]:
					finalRoad += a2_road[(i+1)%size];
					break;

	a = Ant(a1.m_name+a2.m_name, finalRoad[0], a1.m_graph, a1.m_usePhero);
	a.m_old_road = finalRoad;
	return a;


################################################################################
# @brief Permet de déterminer si un chemin donné est correct
# @param[in] road		{Str} Le chemin a tester
# @return {Bool} TRUE si le chemin est correct. FALSE sinon
##
def isGoodRoad( road ):
	for i in xrange(0,len(road)):
		for j in xrange(0,len(road)):
			if i != j and road[i] == road[j]:
				return False;
	return True;


################################################################################
# @brief Permet de supprimer les vieilles fourmi
# @param[in,out] antList		{List<Ant>} List de fourmi
# @param[in] __PARAM__			{Dict} Les paramètres du système
# @return[NONE]
##
def removeOldAnt( antList, __PARAM__ ):
	if not __PARAM__['use_genetic'] or not __PARAM__['genetic']['removeOldAnt']:
		return;
	i = 0;
	while True:
		if antList[i].m_longevity >= __PARAM__['genetic']['longevity']:
			del antList[i];
		else:
			i+=1;

		if i not in antList:
			return;


################################################################################
# @brief Permet de parser la ligne de commande
# @param[in] argv		{List} Argv :D
# @param[in,out] params	{Dict} Les paramètres du système
# @return[NONE]
##
def parseArgv( argv, params ):
	for cmd in argv:
		if cmd == argv[0]:
			continue;
		cmd = cmd.split('=');
		if cmd[0] in params:
			if type(params[cmd[0]]) == int:
				try:
					params[cmd[0]] = int(cmd[1]);
				except:
					print 'Type incorrect pour <'+cmd[0]+'> attendu '+str(type(params[cmd[0]]));
					sys.exit(1);
				if params[cmd[0]] < 0:
					print 'Valeur negative impossible !';
					sys.exit(1);
			elif type(params[cmd[0]]) == bool:
				try:
					cmd[1] = cmd[1].lower();
					params[cmd[0]] = cmd[1] == 'true';
				except:
					print 'Type incorrect pour <'+cmd[0]+'> attendu '+str(type(params[cmd[0]]));
					sys.exit(1);
			else:
				print 'Type non reconnu pour <'+cmd[0]+'> attendu '+str(type(params[cmd[0]]));
				sys.exit(1);

		elif cmd[0] in params['genetic']:
			if type(params['genetic'][cmd[0]]) == int:
				try:
					params['genetic'][cmd[0]] = int(cmd[1]);
				except:
					print 'Type incorrect pour <'+cmd[0]+'> attendu '+str(type(params['genetic'][cmd[0]]));
					sys.exit(1);
				if params['genetic'][cmd[0]] < 0:
					print 'Valeur negative impossible !';
					sys.exit(1);
			elif type(params['genetic'][cmd[0]]) == bool:
				try:
					cmd[1] = cmd[1].lower();
					params['genetic'][cmd[0]] = cmd[1] == 'true';
				except:
					print 'Type incorrect pour <'+cmd[0]+'> attendu '+str(type(params['genetic'][cmd[0]]));
					sys.exit(1);
			else:
				print 'Type non reconnu pour <'+cmd[0]+'> attendu '+str(type(params['genetic'][cmd[0]]));
				sys.exit(1);

		elif cmd[0] == '-h' or cmd[0] == '-help' or cmd[0] == '--help' or len(cmd) == 1:
			print 'Help';
			print 'nbIter			{Uint} Nombre d iteration (default:'+str(params['nbIter'])+')';
			print 'nbAnts			{Uint} Nombre de fourmies (default:'+str(params['nbAnts'])+')';
			print 'use_phero		{Bool} Utiliser les pheromones ?  (default:'+str(params['use_phero'])+')';
			print 'use_genetic		{Bool} Utiliser l algo genetique ?  (default:'+str(params['use_genetic'])+')';
			print 'isCrossing		{Bool} Activer le crossing ? (Activable uniquement si use_genetic=True  (default:'+str(params['genetic']['isCrossing'])+')';
			print 'nbCrossing		{Uint} Nombre de crossing ? (Activable uniquement si use_genetic=True et isCrossing=True  (default:'+str(params['genetic']['nbCrossing'])+')';
			print 'nbMutation		{Uint} Nombre de mutation ? (Activable uniquement si use_genetic=True  (default:'+str(params['genetic']['nbMutation'])+')';
			print 'removeOldAnt		{Bool} Supprimer les vieille fourmies ? (Activable uniquement si use_genetic=True  (default:'+str(params['genetic']['removeOldAnt'])+')';
			print 'longevity		{Uint} Age d une vieille fourmie ? (Activable uniquement si use_genetic=True et removeOldAnt=True  (default:'+str(params['genetic']['longevity'])+')';
			print 'disablePrint		{Bool} Desactiver l affichage ? (default:'+str(params['disablePrint'])+')';
			print 'disableWind		{Bool} Desactiver le vent ? (Activable uniquement si use_prero=True) (default:'+str(params['disableWind'])+')';
			print 'makeRandomWind		{Bool} Le vent doit etre aleatoire ? (Activable uniquement si use_prero=True et disableWind=False) (default:'+str(params['makeRandomWind'])+')';
			print 'Utilisation d\'une option, exemple: nbIter=500 removeOldAnt=True';
			sys.exit(0);

		else:
			print '<'+cmd[0]+'> argument inconnu !';
			sys.exit(1);


################################################################################
# @brief Permet d'afficher un dico de façon propre
# @param[in] dico		{Dict} Le dico a afficher
# @param[in] width		{Uint} Largeur Max
# @param[in,opt] tab	{Str} Tabulations a ajouter
# @return[NONE]
##
def printDict( dico, width, tab='' ):
	for i in dico:
		if type(dico[i]) == dict:
			printDict(dico[i], width, tab+'\t');
		else:
			print ('%-'+str(width)+'s %s')%(tab+i, str(dico[i]))


if __name__ == '__main__':
	ret = main(sys.argv);
	if ret:
		sys.exit(ret);
