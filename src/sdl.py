#!/usr/bin/python
#coding= utf8
################################################################################
# Ce fichier contient le système d'affichage SDL
# /!\ AUCUN calcule vital n'est effectué ici !
#
# Information sur le clavier:
#	ESPACE	= pause/reprendre
#	ESC		= Quitter
#	+		= Augmenter la vitesse
#	-		= Diminuer la vitesse
#	b		= Afficher / cacher le meilleur chemin
#	c		= Afficher / cacher le cercle
##
from algo_ant import __GRAPH__, Graph, Ant, main as algo;
import sys, os;
from math import floor, cos, sin, sqrt;
from time import sleep;
import pygame, pygame.gfxdraw;

# Taille de la window
__WINDOW_SIZE__ = 600,480;
# Couleur du texte
__TEXT_COLOR__ = (0,0,0);
# Couleur du meilleur chemin
__BEST_LINE_COLOR__ = (0,255,0);
# Marge
__MARGIN__ = 20;
# Niveau d'épasseur MAXI pour un niveau de phéro MAXI
__MAX_PHERO__ = 5;
# Afficher le cercle de positionnement ?
__SHOW_CIRCLE__ = True;
# Vitesse de départ
__SPEED__ = 0.1;
# Afficher les infos sur le graph à gauche.
__SHOW_INFO__ = True;


################################################################################
## Ne pas toucher
__SCREEN__ = None;
__FONT__ = None;
__CIRCLE__ = {};
__MAX_TEXT_WIDTH__ = 0;
__SPEED_ptr__ = None;
__PAUSE_ptr__ = None;
__SHOW_best_road__ = True;
__BEST_ROAD__ = {'ptr':None,'road':''};
__PARAM__ = {
	'nbIter':		100,#!< Nombre max d'iter
	'nbAnts':		10,#!< Nombre de fourmi. Doit être supperieur à len(__GRAPH__)
	'use_genetic':	False,#!< Utiliser l'ago génétique ?
	'use_phero':	True,#!< Activer le système de phéromones ?
	'genetic':{
		'isCrossing':	True,#!< Activer la réplication ?
		'nbMutation':	2,#!< Nombre de mutation
		'nbCrossing':	2,#!< Nombre de fourmi à générer
		'removeOldAnt':	True,#!< Supprimer les vieilles fourmis ?
		'longevity':	10#!< Durée de vie des fourmis
	},
	'disablePrint':	False,#!< Désactiver l'affichage
	'disableWind':	False,#!< Désactiver le vent
	'makeRandomWind': False# Vent aléatoire ?
};


################################################################################
# @brief Système d'affichage et de gestion du clavier
# @param[in] gr		{Graph} Le graph a afficher
# @return[NONE]
##
def toPrint( gr ):
	global __SPEED__, __SPEED_ptr__, __SHOW_best_road__, __SHOW_CIRCLE__, __SHOW_INFO__;
	isInPause = False;
	while 1:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit();
				sys.exit();

			if event.type == pygame.KEYDOWN:
				if event.key == 270:# +
					if __SPEED__ <= 0.1:
						__SPEED__ -= 0.01;
					else:
						__SPEED__ -= 0.1;

					# Anti Bug
					if __SPEED__ == 0.0:
						__SPEED__ = 0.1;

					__SPEED_ptr__ = genText('x'+str(__SPEED__));

				elif event.key == 269:# -
					if __SPEED__ < 0.1:
						__SPEED__ += 0.01;
					else:
						__SPEED__ += 0.1;

					__SPEED_ptr__ = genText('x'+str(__SPEED__));

				elif event.key == 32:# Espace
					isInPause = not isInPause;
					print 'Pause Toogle';
					pygame.display.update(printSurfaceBottomCenter(__PAUSE_ptr__));

				elif event.key == 27:# Echap
					pygame.quit();
					sys.exit();

				elif event.key == 98:# B
					__SHOW_best_road__ = not __SHOW_best_road__;

				elif event.key == 99 and not gr.m_has2DPoints:# C
					__SHOW_CIRCLE__ = not __SHOW_CIRCLE__;

				elif event.key == 116:# T
					__SHOW_INFO__ = not __SHOW_INFO__;

		if not isInPause:
			break;
		else:
			sleep(0.1);

	sleep(__SPEED__);

	__SCREEN__.fill((255,255,255))# Fond Blanc
	for i in gr.m_2dCoord:
		__SCREEN__.blit(gr.m_2dCoord[i]['ptr'], gr.m_2dCoord[i]['coord']);

	maxPhero = gr.maxPhero();
	if maxPhero == 0:
		maxPhero = 1;

	for i in gr.m_graph:
		if __PARAM__['use_phero']:
			# Ligne de phéro
			phero = gr.m_graph[i]['phero']*__MAX_PHERO__/maxPhero;
			if phero >= 1.0:
				if __SHOW_best_road__ and gr.isInBestRoad(i):
					pygame.draw.line(__SCREEN__, __BEST_LINE_COLOR__, gr.m_2dCoord[i[0]]['coord'], gr.m_2dCoord[i[1]]['coord'], int(round(phero)));
				else:
					pygame.draw.line(__SCREEN__, __TEXT_COLOR__, gr.m_2dCoord[i[0]]['coord'], gr.m_2dCoord[i[1]]['coord'], int(round(phero)));
			else:
				if __SHOW_best_road__ and gr.isInBestRoad(i):
					pygame.draw.aaline(__SCREEN__, __BEST_LINE_COLOR__, gr.m_2dCoord[i[0]]['coord'], gr.m_2dCoord[i[1]]['coord']);
				else:
					# On place une ligne rouge pour montre qu'il n'y a rien
					pygame.draw.aaline(__SCREEN__, (255,0,0), gr.m_2dCoord[i[0]]['coord'], gr.m_2dCoord[i[1]]['coord']);
		else:
			if __SHOW_best_road__ and gr.isInBestRoad(i):
				pygame.draw.aaline(__SCREEN__, __BEST_LINE_COLOR__, gr.m_2dCoord[i[0]]['coord'], gr.m_2dCoord[i[1]]['coord']);
			else:
				# On place une ligne rouge pour montre qu'il n'y a rien
				pygame.draw.aaline(__SCREEN__, __TEXT_COLOR__, gr.m_2dCoord[i[0]]['coord'], gr.m_2dCoord[i[1]]['coord']);


	# Filtre
	def tmp( key ):
		return gr.m_graph[key]['phero'];

	if __SHOW_INFO__ and __PARAM__['use_phero']:
		# Affichage des info: AB {dist} {Phero}
		pos = 0;
		for i in sorted(gr.m_graph, key=tmp, reverse=True):
			# On affiche le poid sur chaque arrête
			imgSize = gr.m_graph[i]['ptr'].get_size();
			__SCREEN__.blit(gr.m_graph[i]['ptr'], (5, pos*imgSize[1]));
			__SCREEN__.blit(genText(str(int(round(gr.m_graph[i]['phero'])))), (10+__MAX_TEXT_WIDTH__, pos*imgSize[1]));
			pos+= 1;

	# Affichage du cercle
	if __SHOW_CIRCLE__:
		pygame.gfxdraw.circle(__SCREEN__, __CIRCLE__['center'][0], __CIRCLE__['center'][1], __CIRCLE__['rayon'], (255,0,0));

	# On affiche la vitesse
	size = __SPEED_ptr__.get_size();
	__SCREEN__.blit(__SPEED_ptr__, (__WINDOW_SIZE__[0]-size[0], __WINDOW_SIZE__[1]-size[1]));

	# On actualise l'affichage du meilleur chemin si nécéssaire
	if __BEST_ROAD__['road'] != str(gr.m_bestRoad):
		__BEST_ROAD__['road'] = str(gr.m_bestRoad);
		__BEST_ROAD__['ptr'] = '';
		for i in gr.m_bestRoad['road']:
			__BEST_ROAD__['ptr'] += i+',';
		__BEST_ROAD__['ptr'] = genText(__BEST_ROAD__['ptr'][:-1]+':'+str(int(round(gr.m_bestRoad['weight']))));

	# On affiche le meilleur chemin
	printSurfaceBottomCenter(__BEST_ROAD__['ptr'], heightMargin=-1*__SPEED_ptr__.get_size()[1]);

	pygame.display.flip();



################################################################################
# @brief MAIN
# @param[in] argv	Les arguments en lignes de commande
# @return {int/None} Une nombre != 0 en cas de probleme. NONE sinon (OU 0)
##
def main( argv ):
	global __SCREEN__, __FONT__, __MAX_TEXT_WIDTH__, __SPEED_ptr__, __PAUSE_ptr__, __SHOW_CIRCLE__;
	gr = Graph(__GRAPH__);

	pygame.init();
	__SCREEN__ = pygame.display.set_mode(__WINDOW_SIZE__);
	__SCREEN__.fill((255,255,255))# Fond Blanc
	# On init le système d'écriture
	__FONT__ = pygame.font.Font(pygame.font.match_font('Ubuntu',True),20);

	__CIRCLE__['rayon'] = (min(__WINDOW_SIZE__)/2)-__MARGIN__;
	__CIRCLE__['center'] = (__WINDOW_SIZE__[0]/2, __WINDOW_SIZE__[1]/2);

	if gr.m_has2DPoints:
		widthMin = 0;
		widthMax = 0;
		heightMin = 0;
		heightMax = 0;
		for i in gr.m_2dCoord:
			gr.m_2dCoord[i]['ptr'] = genText( i, color=__TEXT_COLOR__ );

			# On détermine les min/max de la représentation
			if widthMin > gr.m_2dCoord[i]['coord'][0]:
				widthMin = gr.m_2dCoord[i]['coord'][0];
			if widthMax < gr.m_2dCoord[i]['coord'][0]:
				widthMax = gr.m_2dCoord[i]['coord'][0];
			if heightMin > gr.m_2dCoord[i]['coord'][1]:
				heightMin = gr.m_2dCoord[i]['coord'][1];
			if heightMax < gr.m_2dCoord[i]['coord'][1]:
				heightMax = gr.m_2dCoord[i]['coord'][1];

		# On ajuste la position des points
		for i in gr.m_2dCoord:
			gr.m_2dCoord[i]['coord'][0] = (((gr.m_2dCoord[i]['coord'][0]-widthMin)*(__WINDOW_SIZE__[0]-__MARGIN__*2))/(widthMax-widthMin))+widthMin+__MARGIN__;
			gr.m_2dCoord[i]['coord'][1] = (((gr.m_2dCoord[i]['coord'][1]-heightMin)*(__WINDOW_SIZE__[1]-__MARGIN__*2))/(heightMax-heightMin))+heightMin+__MARGIN__;
		# On désactive l'affichage du cercle dans ce cas là
		__SHOW_CIRCLE__ = False;

	else:
		ptsScale = (360/(gr.m_nbPoint+1))*-1;# Note: *-1 Pour Afficher les points dans le sens des aiguiles d'une montre
		j=0;
		# On place les points tout autour d'un cercle
		for i in gr.m_2dCoord:
			gr.m_2dCoord[i]['ptr'] = genText( i, color=__TEXT_COLOR__ );
			x = cos(ptsScale*j)*__CIRCLE__['rayon']+__CIRCLE__['center'][0];
			y = sin(ptsScale*j)*__CIRCLE__['rayon']+__CIRCLE__['center'][1];
			gr.m_2dCoord[i]['coord'] = (x,y);
			j+= 1;

	#  On génère le texte: AB 5, CB 8, ...
	for i in gr.m_graph:
		gr.m_graph[i]['ptr'] = genText( i+': '+str(int(round(gr.m_graph[i]['weight']))), color=__TEXT_COLOR__ );
		if __MAX_TEXT_WIDTH__ < gr.m_graph[i]['ptr'].get_size()[0]:
			__MAX_TEXT_WIDTH__ = gr.m_graph[i]['ptr'].get_size()[0];

	# On génère l'affichage de la vitesse
	__SPEED_ptr__ = genText('x'+str(__SPEED__));
	# On génère l'affichage du msg PAUSE
	__PAUSE_ptr__  = genText('PAUSE', color=(255,0,0));

	#toPrint();
	algo(argv, toPrintFunct=toPrint, graph=gr, params=__PARAM__);
	toPrint(gr);

	# Message de Fin
	end = genText('FIN', color=(255,0,0));
	pygame.display.update(printSurfaceBottomCenter(end));

	while 1:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit();
				sys.exit();

			if event.type == pygame.KEYDOWN and event.key == 27:# Echap
				pygame.quit();
				sys.exit();
		sleep(0.1);


################################################################################
# @brief permet de charger un texte en mémoire
# @param[in] text	{Str} Texte a afficher
# @param[in] color	{List} Couleur du texte en RGB
# @return {Surface} Le texte chargé
##
def genText( text, color=(0,0,0) ):
	return __FONT__.render(text,True,color);


################################################################################
# @brief Permet d'afficher une surface en bas au center de la fenetre
# @param[in] surface	{Surface} Surface a afficher
# @return {Rect} Position de la surface
#
# @note Pour raffraichir juste cette zone précise:
#	pygame.display.update(printSurfaceBottomCenter(MY_SURFACE));
##
def printSurfaceBottomCenter( suface, widthMargin=0, heightMargin=0 ):
	rect = suface.get_rect();# Format: (x,y, width,height)
	rect = rect.move((__WINDOW_SIZE__[0]-rect[2])/2+widthMargin, __WINDOW_SIZE__[1]-rect[3]+heightMargin);
	__SCREEN__.blit(suface, (rect[0], rect[1]));
	return rect;



if __name__ == '__main__':
	ret = main(sys.argv);
	if ret:
		sys.exit(ret);
